# TP2 : Network boot

## Sommaire

- [TP2 : Network boot](#tp2--network-boot)
  - [Sommaire](#sommaire)
- [0. Setup](#0-setup)
- [I. Installation d'un serveur DHCP](#i-installation-dun-serveur-dhcp)
- [II. Installation d'un serveur TFTP](#ii-installation-dun-serveur-tftp)
- [III. Un peu de conf](#iii-un-peu-de-conf)
- [IV. Installation d'un serveur Apache](#iv-installation-dun-serveur-apache)
- [V. Test](#v-test)

# 0. Setup

➜ Je vous conseille de pop la VM avec Vagrant, mais comme vous voulez en vrai !

➜ Toujours sur Rocky Linux. Une seule VM où on installe tout.

# I. Installation d'un serveur DHCP

🌞 **Installer le paquet `dhcp-server`**

🌞 **Configurer le serveur DHCP**

- il doit filer des IPs dans la bonne range
- la conf doit aussi contenir une section spécifique pour PXE
- je vous mets un p'tit exemple ci-dessous (n'oubliez pas de remplacer les valeurs entre chevrons)

```conf
default-lease-time 600;
max-lease-time 7200;
authoritative;

# PXE specifics
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet <NETWORK_ADDRESS> netmask 255.255.255.0 {
    # définition de la range pour que votre DHCP attribue des IP entre <FIRST_IP> <LAST_IP>
    range dynamic-bootp <FIRST_IP> <LAST_IP>;
    
    # add follows
    class "pxeclients" {
        match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
        next-server <PXE_SERVER_IP_ADDRESS>;

        if option architecture-type = 00:07 {
            filename "BOOTX64.EFI";
        }
        else {
            filename "pxelinux.0";
        }
    }
}
```

🌞 **Démarrer le serveur DHCP**

```bash

[root@ezconf etc]# systemctl start dhcpd

[root@ezconf etc]# systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; disabled; preset: disabled)
     Active: active (running) since Thu 2024-04-04 13:11:07 UTC; 5s ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
   Main PID: 5379 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 12264)
     Memory: 5.2M
        CPU: 30ms
     CGroup: /system.slice/dhcpd.service
             └─5379 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid


```
🌞 **Ouvrir le bon port firewall**

- avec `sudo firewall-cmd --add-service=dhcp --permanent` suivi de `sudo firewall-cmd --reload`

```bash 
[root@ezconf etc]#sudo firewall-cmd --add-service=dhcp --permanent
success

[root@ezconf etc]# sudo firewall-cmd --reload
success
```

# II. Installation d'un serveur TFTP


🌞 **Installer le paquet `tftp-server`**
```bash 
[root@ezconf etc]# dnf install tftp-server

```

🌞 **Démarrer le socket TFTP**

- avec un `sudo systemctl enable --now tftp.socket`

```bash 
[root@ezconf etc]# sudo systemctl enable --now tftp.socket
Created symlink /etc/systemd/system/sockets.target.wants/tftp.socket → /usr/lib/systemd/system/tftp.socket.
```

🌞 **Ouvrir le bon port firewall**

- avec `sudo firewall-cmd --add-service=tftp --permanent` suivi de `sudo firewall-cmd --reload`


```bash 
[root@ezconf etc]# sudo firewall-cmd --add-service=tftp --permanent
success

[root@ezconf etc]# sudo firewall-cmd --reload
success


```

# III. Un peu de conf

Dans cette section, on va récupérer certains fichier contenus dans l'ISO officiel de Rocky Linux, afin de permettre à d'autres machines de les récupérer afin de démarrer un boot sur le réseau.

Avec PXE c'est le délire : on fournit un ISO à travers le réseau, et les machines peuvent l'utiliser pour déclencher une install.

Let's go :

➜ Déjà, récupérez l'iso de Rocky Linux dans la VM.

➜ Ensuite, suivez le guide :

```bash
# on installe les bails nécessaires à l'install d'un nouveao Rocky
dnf -y install syslinux

# on déplace le fichier pxelinux.0 dans le dossier servi par le serveur HTTP/TFTP
cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot/

# on prépare l'environnement
mkdir -p /var/pxe/rocky9
mkdir /var/lib/tftpboot/rocky9

# adaptez avec le chemin vers l'iso de Rocky sur votre VM
mount -t iso9660 -o loop,ro /path/vers/liso/de/rocky.iso /var/pxe/rocky9

# on récupère dans l'iso de Rocky le nécessaire pour démarrer une install
cp /var/pxe/rocky9/images/pxeboot/{vmlinuz,initrd.img} /var/lib/tftpboot/rocky9/
cp /usr/share/syslinux/{menu.c32,vesamenu.c32,ldlinux.c32,libcom32.c32,libutil.c32} /var/lib/tftpboot/

# on prépare le dossier qaui va contenir les options de boot réseau
mkdir /var/lib/tftpboot/pxelinux.cfg
```

➜ Puis, déposez le contenu suivant dans le fichier `/var/lib/tftpboot/pxelinux.cfg/default` :

```conf
default vesamenu.c32
prompt 1
timeout 60

display boot.msg

label linux
  menu label ^Install Rocky Linux 9 my big boiiiiiii
  menu default
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img ip=dhcp inst.repo=http://<IP_DU_SERVEUR_PXE>/rocky9
label rescue
  menu label ^Rescue installed system
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img rescue
label local
  menu label Boot from ^local drive
  localboot 0xffff
```

# IV. Installation d'un serveur Apache

🌞 **Installer le paquet `httpd`**

```bash
[root@ezconf vagrant]# dnf install httpd
```

🌞 **Ajouter un fichier de conf dans `/etc/httpd/conf.d/pxeboot.conf`** avec le contenu suivant :

```apache
Alias /rocky9 /var/pxe/rocky9
<Directory /var/pxe/rocky9>
    Options Indexes FollowSymLinks
    # access permission
    Require ip 127.0.0.1 10.1.1.0/24 # remplace 10.1.1.0/24 par le réseau dans lequel se trouve le serveur
</Directory>
```

🌞 **Démarrer le serveur Apache**

```bash
[root@ezconf vagrant]# systemctl start httpd.service

[root@ezconf vagrant]# systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; preset: disabled)
     Active: active (running) since Thu 2024-04-04 14:07:23 UTC; 4s ago
       Docs: man:httpd.service(8)
   Main PID: 3287 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 12264)
     Memory: 40.1M
        CPU: 323ms
     CGroup: /system.slice/httpd.service
             ├─3287 /usr/sbin/httpd -DFOREGROUND
             ├─3288 /usr/sbin/httpd -DFOREGROUND
             ├─3289 /usr/sbin/httpd -DFOREGROUND
             ├─3290 /usr/sbin/httpd -DFOREGROUND
             └─3291 /usr/sbin/httpd -DFOREGROUND


```

🌞 **Ouvrir le bon port firewall**

- avec `sudo firewall-cmd --add-port=80/tcp --permanent` suivi de `sudo firewall-cmd --reload`

```bash
[root@ezconf vagrant]# sudo firewall-cmd --add-port=80/tcp --permanent
success

[root@ezconf vagrant]# sudo firewall-cmd --reload
success
```

# V. Test

Pour tester, simple :

- vous ouvrez VirtualBox à la main
- vous créez une nouvelle VM
  - pas un clone
  - vous lui mettez pas d'ISO ni rien non plus
  - genre une machine avec un disque vierge
- il faut qu'elle ait une interface dans le même réseau host-only que votre serveur PXE pour pouvoir le contacter
- vous allumez la VM
- une install de Rocky est censée se lancer

🌞 **Analyser l'échange complet avec Wireshark**

- le mieux pour réaliser la capture est sûrement d'utiliser `tcpdump` depuis le serveur PXE
