# TP1 : Programmatic provisioning

## Sommaire

- [TP1 : Programmatic provisioning](#tp1--programmatic-provisioning)
  - [Sommaire](#sommaire)
- [I. Une première VM](#i-une-première-vm)
  - [1. ez startup](#1-ez-startup)
  - [2. Un peu de conf](#2-un-peu-de-conf)
- [II. Initialization script](#ii-initialization-script)
- [III. Repackaging](#iii-repackaging)
- [IV. Multi VM](#iv-multi-vm)
- [V. cloud-init](#v-cloud-init)


# I. Une première VM

## 1. ez startup


🌞 **`Vagrantfile` dans le dépôt git de rendu SVP !**

```bash

Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
end

```



## 2. Un peu de conf

Avec Vagrant, il est possible de gérer un certains nombres de paramètres de la VM.

🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :

- ait l'IP `10.1.1.11/24`
- porte le hostname `ezconf.tp1.efrei`
- porte le nom (pour Vagrant) `ezconf.tp1.efrei` (ce n'est pas le hostname de la machine)
- ait 2G de RAM
- ait un disque dur de 20G

```bash

Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.customize ["modifyvm", :id, "--name", "ezconf.tp1.efrei"]

    current_dir    = File.dirname(File.expand_path(__FILE__))
    disk_prefix = 'ezconf_disk'
    disk_ext ='.vdi'
    disk =  "%s/%s%s" % [current_dir,disk_prefix, disk_ext]
    unless File.exist?(disk)
   	  vb.customize ['createhd', '--filename', disk, '--size', 20480]
    end
	    vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disk]
  end
  config.vm.provision "shell", path: "script.sh"
end

```

# II. Initialization script


🌞 **Ajustez le `Vagrantfile`** :

- quand la VM démarre, elle doit exécuter un script bash
- le script installe les paquets `vim` et `python3`
- il met aussi à jour le système avec un `dnf update -y` (si c'est trop long avec le réseau de l'école, zappez cette étape)
- ça se fait avec une ligne comme celle-ci :


```bash
# on suppose que "script.sh" existe juste à côté du Vagrantfile
config.vm.provision "shell", path: "script.sh"
```

Script.sh
```bash
#!/bin/bash
dnf update -y
dnf install -y vim python3

```

# III. Repackaging

🌞 **Repackager la VM créée précédemment**

```bash
huein@LAPTOP-0DEHFD8B MINGW64 ~/toto
$ vagrant package --output rocky-efrei.box
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/huein/toto/rocky-efrei.box


huein@LAPTOP-0DEHFD8B MINGW64 ~/toto
$ vagrant box add rocky-efrei rocky-efrei.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'rocky-efrei' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/huein/toto/rocky-efrei.box
    box:
==> box: Successfully added box 'rocky-efrei' (v0) for ''!


huein@LAPTOP-0DEHFD8B MINGW64 ~/toto
$ vagrant box list
generic/rocky9 (virtualbox, 4.3.12, (amd64))
rocky-efrei    (virtualbox, 0)
```

# IV. Multi VM

🌞 **Un deuxième `Vagrantfile` qui définit** :

- une VM `node1.tp1.efrei`
  - IP `10.1.1.101/24`
  - 2G de RAM
- une VM `node2.tp1.efrei`
  - IP `10.1.1.102/24`
  - 1G de RAM
  
```bash 
Vagrant.configure("2") do |config|
  config.vm.define "node1" do |node1|
    node1.vm.box = "generic/rocky9"
    node1.vm.network "private_network", ip: "10.1.1.101"
    node1.vm.hostname = "node1.tp1.efrei"
    node1.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
    end
  end

  config.vm.define "node2" do |node2|
    node2.vm.box = "generic/rocky9"
    node2.vm.network "private_network", ip: "10.1.1.102"
    node2.vm.hostname = "node2.tp1.efrei"
    node2.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
    end
  end
end
```


🌞 **Une fois les VMs allumées, assurez-vous que vous pouvez ping `10.1.1.102` depuis `node1`**
```bash
$ vagrant status
Current machine states:

node1                     running (virtualbox)
node2                     running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.


$ vagrant ssh node1
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=2.68 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=6.83 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=2.84 ms
^C
--- 10.1.1.102 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2014ms
rtt min/avg/max/mdev = 2.677/4.115/6.826/1.917 ms

```


# V. cloud-init


Vagrantfile
```bash
Vagrant.configure("2") do |config|
  config.vm.box = "rocky-efrei.box"
  config.vm.cloud_init :user_data, content_type: "text/cloud-config", path: "user_data.yml"
end
```
user_data.yml
```bash
#cloud-config
users:
  - name: huein
    gecos: Super adminsys
    primary_group: huein
    groups: wheel
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$o.ateVLe$Hz2mP7.ImTfK67wOtWkI3.bS6zkY/gpjWOVLjDkYeFzyFE0jj6BsO78umKCi40EewjFc6rZMtKDieQPDt4gzt/
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDyl8Dud71ELLs2QX0b84LBgvCYvVrFp1FMf+sAHzKc875kPE6SA7dVRaF4dZqz0T2YKgVZj22VHEIUT4WlDEcTh3rktCmJ8cIe67EsFVSDKZTwVdBRbw3KJcsPyhruSVaI1U+liECGV5vE243upG86StjNozf+Nsy4zWo0h9+5o/YjUkLqTlB8TtkEh9oc6C3W6tY9Of1Elnv6ac+X5pYnUjw8LS/xn7P2ZlP1V3oM7GJLjvm/WlhEr1O5l9QkAtWgdtutfvvZMEbg4lIn6WjQrA17teCoPRoKZz2zAhIhi4VfPyCM/UU4pUBR7YskQIb64R1bM5mnfcsabeyF71EJsgRPBxyNxYWLVwvFkswAkDTHso4dom8DTioOj1k2zZI2zSFjea7RdhUpAIe8VIDgqrhtXPqM9Zzhk5cc/vBjC7U6idL2OB9+BtkU5rdTcT52Z2i7sCo+jiySBYhaYsYRAY7jnf8oibGS1240uX+7oadSjT1S6oeKhEFqlSeY/9EfmEr0qZpMfjGTf2UAD3i/KCrMQnMgtotNj7NArwqCGLsk8tEJoV60V7ybGbLpnC5m3xQtPomZJAIatX7Ff8OBl05+jiZ8exri2Fw0hd+zJvezTHL+7PWDAfDr216eF4YGs9jAHy3wzhWOUshj8EHPzcVwm9VbCafDvzTZHOqxw== huein@LAPTOP-0DEHFD8B

```


